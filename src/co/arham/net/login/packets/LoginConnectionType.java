package co.arham.net.login.packets;

import co.arham.net.login.LoginPacket;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import lombok.Getter;

/**
 * Sends the type of connection that is about to go through. Normal or reconnection?
 *
 * @author Arham 4
 */
public class LoginConnectionType extends LoginPacket implements KryoSerializable {

    @Override
    public void write(Kryo kryo, Output output) {
        if (LoginResponse.getLoginResponseCode() == LoginResponse.LoginResponseCode.EXCHANGE_DATA) {
            output.writeByte(ConnectionType.NORMAL.getId());
            // TODO reconnection
            // TODO is reconnection really necessary though?
        }
    }

    @Override
    public void read(Kryo kryo, Input input) {

    }

    /**
     * I read somewhere that you should probably use enums over constants. I personally agree.
     */
    private enum ConnectionType {
        NORMAL(16),
        RECONNECTING(18);

        @Getter
        private final int id;

        ConnectionType(int id) {
            this.id = id;
        }
    }
}
