package co.arham.net.login.packets;

import co.arham.net.Networking;
import co.arham.net.login.LoginPacket;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.jagex.Client;
import com.jagex.sign.SignLink;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Represents the login responses sent between the client and server during the login process.
 *
 * @author Arham 4
 */
public class LoginResponse extends LoginPacket implements KryoSerializable {

    @Getter
    private static LoginResponseCode loginResponseCode;
    private long serverSeed;

    @Override
    public void write(Kryo kryo, Output output) {
        switch (loginResponseCode) {
            case EXCHANGE_DATA:
                int[] seeds = new int[4];
                seeds[0] = (int) (Math.random() * 99999999D);
                seeds[1] = (int) (Math.random() * 99999999D);
                seeds[2] = (int) (serverSeed >> 32);
                seeds[3] = (int) serverSeed;
                output.writeByte(10);
                for (int seed : seeds) {
                    output.writeInt(seed);
                }
                output.writeInt(SignLink.getUid());
                output.writeString(Client.getUsername());
                output.writeString(Client.getPassword());
                //TODO RSA
                break;
        }
    }

    @Override
    public void read(Kryo kryo, Input input) {
        int opcode = input.readByte();
        loginResponseCode = LoginResponseCode.getLoginResponseCode(opcode);
        handleLoginResponse(input);
    }

    /**
     * Some may ask, why not just assign the login messages in the enumeration? Well, to be honest, I didn't want to be
     * putting "null" in some of the values. I know you could always do "if loginResponseCode message is null, do this,"
     * but I'd prefer it to be like this. Call it repetitive.
     */
    private void handleLoginResponse(Input input) {
        switch (loginResponseCode) {
            case EXCHANGE_DATA:
                //serverSeed = input.readLong();
                break;
            case INVALID_CREDENTIALS:
                Networking.getClient().setLoginMessages("", "Invalid username or password.");
                break;
            case ACCOUNT_DISABLED:
                Networking.getClient().setLoginMessages("Your account has been disabled.", "Please check your message-centre for details.");
                break;
            case ACCOUNT_ONLINE:
                Networking.getClient().setLoginMessages("Your account is already logged in.", "Try again in 60 secs...");
                break;
            case GAME_UPDATED:
                Networking.getClient().setLoginMessages("RuneScape has been updated!", "Please reload this page.");
                break;
            case WORLD_FULL:
                Networking.getClient().setLoginMessages("This world is full.", "Please use a different world.");
                break;
            case LOGIN_SERVER_OFFLINE:
                Networking.getClient().setLoginMessages("Unable to connect.", "Login server offline.");
                break;
            case LOGIN_LIMIT_EXCEEDED:
                Networking.getClient().setLoginMessages("Login limit exceeded.", "Too many connections from your address.");
                break;
            case BAD_SESSION_ID:
                Networking.getClient().setLoginMessages("Unable to connect.", "Bad session id.");
                break;
            case LOGIN_SERVER_REJECTED_SESSION:
                Networking.getClient().setLoginMessages("Login server rejected session.", "Please try again.");
                break;
            case MEMBERS_ACCOUNT_REQUIRED:
                Networking.getClient().setLoginMessages("You need a members account to login to this world.", "Please subscribe, or use a different world.");
                break;
            case COULD_NOT_COMPLETE_LOGIN:
                Networking.getClient().setLoginMessages("Could not complete login.", "Please try using a different world.");
                break;
            case SERVER_BEING_UPDATED:
                Networking.getClient().setLoginMessages("The server is being updated.", "Please wait 1 minute and try again.");
                break;
            case LOGIN_ATTEMPTS_EXCEEDED:
                Networking.getClient().setLoginMessages("Login attempts exceeded.", "Please wait 1 minute and try again.");
                break;
            case MEMBERS_ONLY_AREA:
                Networking.getClient().setLoginMessages("You are standing in a members-only area.", "To play on this world move to a free area first");
                break;
            case INVALID_LOGIN_SERVER:
                Networking.getClient().setLoginMessages("Invalid loginserver requested", "Please try using a different world.");
                break;
        }
    }

    /**
     * Represents the enumerated login response.
     *
     * @author SeVen
     */
    public enum LoginResponseCode {
        EXCHANGE_DATA(0),
        DELAY(1),
        NORMAL(2),
        INVALID_CREDENTIALS(3),
        ACCOUNT_DISABLED(4),
        ACCOUNT_ONLINE(5),
        GAME_UPDATED(6),
        WORLD_FULL(7),
        LOGIN_SERVER_OFFLINE(8),
        LOGIN_LIMIT_EXCEEDED(9),
        BAD_SESSION_ID(10),
        LOGIN_SERVER_REJECTED_SESSION(11),
        MEMBERS_ACCOUNT_REQUIRED(12),
        COULD_NOT_COMPLETE_LOGIN(13),
        SERVER_BEING_UPDATED(14),
        RECONNECTING(15),
        LOGIN_ATTEMPTS_EXCEEDED(16),
        MEMBERS_ONLY_AREA(17),
        INVALID_LOGIN_SERVER(20),
        TRANSFERRING_PROFILE(21);

        @Getter
        private final int opcode;

        private static final List<LoginResponseCode> VALUES = Collections.unmodifiableList(Arrays.asList(values()));

        LoginResponseCode(int opcode) {
            this.opcode = opcode;
        }

        public static LoginResponseCode getLoginResponseCode(int opcode) {
            for (LoginResponseCode loginResponseCode : VALUES) {
                if (loginResponseCode.getOpcode() == opcode) {
                    return loginResponseCode;
                }
            }
            return LoginResponseCode.COULD_NOT_COMPLETE_LOGIN;
        }
    }
}
