package co.arham.net.login.packets;

import co.arham.net.login.LoginPacket;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryonet.Connection;
import com.jagex.Client;
import com.jagex.util.StringUtils;
import lombok.Getter;

/**
 * A singular request that will be sent to the server requesting to login.
 *
 * @author Arham 4
 */
public class LoginRequest extends LoginPacket implements KryoSerializable {

    @Override
    public void write(Kryo kryo, Output output) {
        long encoded = StringUtils.encodeBase37(Client.getUsername());
        int nameHash = (int) (encoded >> 16 & 31L);
        output.writeByte(Request.GAME_SERVER.getId());
        output.writeByte(nameHash);
    }

    @Override
    public void read(Kryo kryo, Input input) {
        input.readLong();
        input.readByte();
        input.readLong();
    }

    @Override
    public void received(Connection connection) {
        connection.sendTCP(new LoginConnectionType());
        connection.sendTCP(new LoginBlockSize());
    }

    private enum Request {
        GAME_SERVER(14),
        FILE_SERVER(15);

        @Getter
        private final int id;

        Request(int id) {
            this.id = id;
        }
    }
}
