package co.arham.net.login.packets;

import co.arham.net.login.LoginPacket;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * Sends the login block size before sending the login block to make sure we (the client and the server) are on the
 * same level.
 *
 * @author Arham 4
 */
public class LoginBlockSize extends LoginPacket implements KryoSerializable {

    /**
     * Might be subject to change due to difference in networking.
     */
    private static final int SIZE = 76;

    @Override
    public void write(Kryo kryo, Output output) {
        output.writeInt(SIZE);
    }

    @Override
    public void read(Kryo kryo, Input input) {

    }
}
