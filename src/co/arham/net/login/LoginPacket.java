package co.arham.net.login;

import com.esotericsoftware.kryonet.Connection;

/**
 * A rather OO approach for packets, so they can be decoupled from the received() method in ServerListener
 *
 * @author Arham 4
 */
public abstract class LoginPacket {

    /**
     * Any better naming for this possibly?
     * <p>
     * Purpose: handles the actions after the packet has been read.
     */
    public void received(Connection connection) {
    }
}
