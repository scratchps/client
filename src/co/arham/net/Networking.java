package co.arham.net;

import co.arham.net.login.packets.LoginBlockSize;
import co.arham.net.login.packets.LoginConnectionType;
import co.arham.net.login.packets.LoginRequest;
import co.arham.net.login.packets.LoginResponse;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.minlog.Log;
import com.jagex.Client;

import java.io.IOException;

/**
 * Handles a majority of the networking related stuff.
 *
 * @author Arham 4
 */
public class Networking {

    /**
     * Unfortunate naming conflict
     */
    private static Client client;
    private com.esotericsoftware.kryonet.Client connectionClient;
    private static Networking networking = new Networking();

    public void connect() {
        try {
            Log.set(Log.LEVEL_DEBUG);
            connectionClient = new com.esotericsoftware.kryonet.Client();
            connectionClient.start();
            connectionClient.connect(30000, "127.0.0.1", 43594);
            connectionClient.addListener(new NetworkingListener());
            registerPackets();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendLoginData() {
        LoginRequest loginRequest = new LoginRequest();
        connectionClient.sendTCP(loginRequest);
    }

    private void registerPackets() {
        Kryo kryo = connectionClient.getKryo();

        kryo.register(LoginRequest.class);
        kryo.register(LoginResponse.class);
        kryo.register(LoginConnectionType.class);
        kryo.register(LoginBlockSize.class);
    }

    public static Networking getSingleton(Client client) {
        Networking.client = client;
        return networking;
    }

    public static Client getClient() {
        return client;
    }
}
