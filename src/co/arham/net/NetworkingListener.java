package co.arham.net;

import co.arham.net.login.LoginPacket;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

/**
 * Listens to the packets and such.
 *
 * @author Arham 4
 */
public class NetworkingListener extends Listener {

    @Override
    public void received(Connection connection, Object object) {
        LoginPacket loginPacket = (LoginPacket) object;
        if (loginPacket != null) {
            loginPacket.received(connection);
        }
    }
}
